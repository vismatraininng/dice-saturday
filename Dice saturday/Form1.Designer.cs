﻿namespace Dice_saturday
{
    partial class DicesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generateDicesButton = new System.Windows.Forms.Button();
            this.reRollButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // generateDicesButton
            // 
            this.generateDicesButton.Location = new System.Drawing.Point(30, 33);
            this.generateDicesButton.Name = "generateDicesButton";
            this.generateDicesButton.Size = new System.Drawing.Size(75, 23);
            this.generateDicesButton.TabIndex = 0;
            this.generateDicesButton.Text = "Generate";
            this.generateDicesButton.UseVisualStyleBackColor = true;
            this.generateDicesButton.Click += new System.EventHandler(this.generateDicesButton_Click);
            // 
            // reRollButton
            // 
            this.reRollButton.Location = new System.Drawing.Point(127, 33);
            this.reRollButton.Name = "reRollButton";
            this.reRollButton.Size = new System.Drawing.Size(75, 23);
            this.reRollButton.TabIndex = 1;
            this.reRollButton.Text = "ReRoll";
            this.reRollButton.UseVisualStyleBackColor = true;
            this.reRollButton.Click += new System.EventHandler(this.reRollButton_Click);
            // 
            // DicesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 308);
            this.Controls.Add(this.reRollButton);
            this.Controls.Add(this.generateDicesButton);
            this.Name = "DicesForm";
            this.Text = "Dices";
            this.Load += new System.EventHandler(this.DicesForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button generateDicesButton;
        private System.Windows.Forms.Button reRollButton;
    }
}

