﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice_saturday
{
    public partial class DicesForm : Form
    {

        List<Dice> dices = new List<Dice>();

        public DicesForm()
        {
            InitializeComponent();
        }

        private void generateDicesButton_Click(object sender, EventArgs e)
        {
            dices = GenerateMockDices(64);
            ShowDices(dices);
        }

        private void ShowDices(List<Dice> dices)
        {
            foreach (var dice in dices)
            {
                ShowDice(dice);
            }
        }
 
        private void ShowDice(Dice dice)
        {
            Label testLabel = new Label();

            testLabel.BackColor = Color.White;
            testLabel.BorderStyle = BorderStyle.FixedSingle;
            testLabel.Font = new Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            testLabel.Location = new Point(dice.X, dice.Y);
            testLabel.Size = new Size(45, 45);
            testLabel.Text = dice.Value.ToString();
            testLabel.TextAlign = ContentAlignment.MiddleCenter;

            testLabel.Tag = dice;

            testLabel.Click += RollADice;
            testLabel.MouseMove += RollADice;

            this.Controls.Add(testLabel);
        }

        private void RollADice(object sender, EventArgs e)
        {
            Dice dice = ((Dice)((Label)sender).Tag);
            dice.Roll();
            ((Label)sender).Text = dice.Value.ToString();
        
        }

        private static List<Dice> GenerateMockDices(int numberOfDices)
        {
            List<Dice> dices = new List<Dice>();

            for (int i = 0; i < numberOfDices; i++)
            {
                dices.Add(new Dice { 
                    Value = 6, 
                    X = 10 + 47*(i%8), 
                    Y = 80 + 47*(i/8) });
            }

            foreach (var dice in dices)
            {
                dice.Roll();
            }

            return dices;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //ShowDice(new Dice { Value = 6, X = 200, Y = 120 });
        }

        private void DicesForm_Load(object sender, EventArgs e)
        {

        }

        private void reRollButton_Click(object sender, EventArgs e)
        {
            foreach (var dice in dices)
            {
                dice.Roll();
            }

            HideDices();
            ShowDices(dices);
        }

        private void HideDices()
        {
            foreach (var control in Controls)
            {
                if (control is Label)
                {
                    ((Label)control).Visible = false;
                }
            }
        }

     

    }
}
